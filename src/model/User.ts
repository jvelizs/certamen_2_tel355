import {Schema, model} from 'mongoose'

const userSchema = new Schema({
    name: {
        type: String
    },
    edad:{
        type: Number
    },
    email: {
        type: String
    },
    favoriteGenres: {
        type: Array
    }

})

export default model('User', userSchema)