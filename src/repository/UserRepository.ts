import User from '../model/User'

export const insertUserRepository = async (name: String, edad: Number, email: String, favoriteGenres: Array<String>) => {
    try{
    const user = new User({ name, edad, email, favoriteGenres })
    await user.save()
    let user_id=user.id
    return {
        status: "OK",
        message: "User created correctly",
        userId: user_id
    }}catch{
        return{
            status: "NOK",
            message: "there was an error creating user"
        }
    }
}

export const deleteUserRepository= async (userId: String)=>{
    const user= await User.exists({_id: userId})
    try{
        if(user){
            User.findByIdAndDelete(userId,function (err) { 
                 if (err){ 
                    console.log(err)
                    }  
                }
            )
        return{
                status:"OK",
                message:"User deleted correctly"
            }
        }

        if(!user){
            return{
                status:"NOK",
                 message:"User not found"
            }
        }
    }catch{
        return{
            status: "NOK",
            message: "there was an error deleting user"
        }
     }
}