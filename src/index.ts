import express, { Express } from 'express'
import bodyParser from 'body-parser'
import mongoose from 'mongoose'

// Controllers
import * as rootController from './controller/rootController'
import * as moviesController from './controller/moviesController'
import * as UserController from './controller/UserController'

const server: Express = express()

server.use(bodyParser.json())
server.use(bodyParser.urlencoded({
    extended: true
}))
//conexion mongobd
mongoose.connect('mongodb://mongo:27017/Certamen_2', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
})

// Services
server.get('/', rootController.sendDeafultMessage)
server.get('/movies', moviesController.sendMoviesInfo)

server.post('/insert', UserController.insertUser)
server.delete('/user/:userId', UserController.deleteUser)
server.listen(3000, () => {
    console.log('Server listening at port 3000')
})