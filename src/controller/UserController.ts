import { Request, Response } from 'express'
import { insertUserRepository, deleteUserRepository} from '../repository/UserRepository'

export const insertUser =async (req: Request, res: Response) => {
    const response= await insertUserRepository(req.body.name, req.body.edad,req.body.email, req.body.favoriteGenres)
    if(response.status=="OK"){
        res.status(200).json(response)
    }
    else{
        res.status(500).json(response)
    }
}
export const deleteUser = async (req: Request, res: Response) => {
    const response = await deleteUserRepository(req.params.userId)
    if(response.status=="OK"){
        res.status(200).json(response)
    }
    else if(response.message=="User not found"){
        res.status(400).json(response)
    }
    else{
        res.status(500).json(response)
    }
}