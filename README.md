## Instrucciones de instalación

```
npm install
```

### Instalación de Typescript

```
npm install -g typescript
```

### Instalación de jest

```
npm install -g jest
```

## Levantando el proyecto

```
npm start
```

## Para iniciar el compose
```
docker-compose build
docker-compose up -d
```
### Se requiere probar el POST y DELETE en postman.
### Se utilizo MongoDB Compass para el caso de visualizar los cambios en la base de datos, asi el trabajo es mas agradable
### No se alcanzo a realizar los test unitatios, ya que al intalar, tenia un error el cual no me pude deshacer asi que opte por subir lo bueno del codigo
## Se realizaron las preguntas 1) y 2)
## se uso el puerto 3000

# Happy coding!!!
